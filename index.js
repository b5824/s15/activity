console.log('Hello World');

const lastName1 = 'Hailey';
let age1 = 23;
let hobbies1 = ['Playing guitar', 'Playing games', 'Browsing stuff'];
let workAddress = {
    houseNumber: '45',
    city: 'London',
    county: 'Morden',
};

console.log(lastName1);
console.log(age1);
console.log(hobbies1);
console.log(workAddress);

//Add your variables and console log for objective 1 here:

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/

let fullName2 = 'Steve Rogers';
console.log('My full name is: ' + fullName2);

let currentAge = 40;
console.log('My current age is:  ' + currentAge);

let friends = ['Tony', 'Bruce', 'Thor', 'Natasha', 'Clint', 'Nick'];
console.log('My Friends are: ');
console.log(friends);

let profile = {
    username: 'Captain America',
    fullName: 'Steve Rogers',
    age: 40,
    isActive: false,
};
console.log('My Full Profile: ');
console.log(profile);

let fullName = 'Bucky Barnes';
console.log('My bestfriend is: ' + fullName);

const lastLocation = 'Arctic Ocean';
console.log('I was found frozen in: ' + lastLocation);